## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| key\_retention\_days | Number of days to store keys in S3 before automatic removal | string | `"2"` | no |
| name | Name to prepend to module resources. | string | n/a | yes |
| play\_ready\_content\_key | Encoded PlayReady Header that is included in the encrypted content | string | `""` | no |
| play\_ready\_header | Encoded PlayReady Header that is included in the encrypted content | string | `""` | no |
| play\_ready\_pssh\_box | Encoded PlayReady Protection System Specific Header box | string | `""` | no |
| tags | Map of resource tags. | map | `<map>` | no |
| widevine\_header | Encoded Widevine Header that is included in the encrypted content | string | `""` | no |
| widevine\_pssh\_box | Encoded Widevine Protection System Specific Header box | string | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| key\_bucket | Bucket name for the SPEKE key cache |
| media\_convert\_speke\_role\_arn | ARN of the role for MediaConvert to invoke the SPEKE server. |
| media\_package\_speke\_role\_arn | ARN of the role for MediaPackage to invoke the SPEKE server. |
| speke\_server\_url | URL for the SPEKE server that is called by MediaPackage. |

