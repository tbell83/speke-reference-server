resource "aws_lambda_function" "speke_server_lambda" {
  count = var.mod_count

  s3_bucket     = var.lambda_s3_bucket
  s3_key        = var.lambda_s3_object
  function_name = "${var.name}-speke_server"
  handler       = var.lambda_handler
  memory_size   = 3008
  role          = join("", aws_iam_role.speke_server_lambda_role.*.arn)
  runtime       = "python3.6"
  timeout       = 15

  environment {
    variables = var.lambda_variables != null ? merge(local.lambda_variables, var.lambda_variables) : local.lambda_variables
  }

  tags = local.tags
}

resource "aws_lambda_permission" "invoke_speke_server_permission" {
  count = var.mod_count

  action        = "lambda:InvokeFunction"
  function_name = join("", aws_lambda_function.speke_server_lambda.*.function_name)
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.execution_arn)}/*/POST/copyProtection"
}
