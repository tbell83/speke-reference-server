locals {
  speke_server_url = "https://${join("", aws_api_gateway_deployment.speke_server_api_deployment.*.id)}.execute-api.${data.aws_region.current.name}.amazonaws.com/${var.stage_name}/copyProtection"
  tags             = var.tags
  lambda_variables = {
    KEYSTORE_BUCKET             = join("", aws_s3_bucket.key_bucket.*.id)
    KEYSTORE_URL                = join("", aws_cloudfront_distribution.key_bucket_cloudfront_distribution.*.domain_name)
    PLAYREADY_PSSH_BOX          = var.play_ready_pssh_box
    PLAYREADY_PROTECTION_HEADER = var.play_ready_header
    PLAYREADY_CONTENT_KEY       = var.play_ready_content_key
    WIDEVINE_PSSH_BOX           = var.widevine_pssh_box
    WIDEVINE_PROTECTION_HEADER  = var.widevine_header
  }
}
