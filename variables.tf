variable "key_retention_days" {
  default     = 2
  description = "Number of days to store keys in S3 before automatic removal"
}

variable "play_ready_content_key" {
  default     = ""
  description = "Encoded PlayReady Header that is included in the encrypted content"
}

variable "play_ready_header" {
  default     = ""
  description = "Encoded PlayReady Header that is included in the encrypted content"
}

variable "play_ready_pssh_box" {
  default     = ""
  description = "Encoded PlayReady Protection System Specific Header box"
}

variable "widevine_header" {
  default     = ""
  description = "Encoded Widevine Header that is included in the encrypted content"
}

variable "widevine_pssh_box" {
  default     = ""
  description = "Encoded Widevine Protection System Specific Header box"
}

variable "tags" {
  default     = {}
  description = "Map of resource tags."
}

variable "name" {
  description = "Name to prepend to module resources."
}

variable "mod_count" {
  default = 1
}

variable "lambda_s3_bucket" {
  description = "S3 Bucket ID where lambda function code is stored."
}
variable "lambda_s3_object" {
  description = "Prefix for Lambda function archive."
}

variable "lambda_handler" {
  description = "Lambda function handler."
}

variable "invoke_gateway_arns" {
  description = "List of IAM ARNs that can invoke the gateway."
  default     = []
}

variable "stage_name" {
  description = "API Gateway stage name."
  default     = "EkeStage"
}

variable "s3_logging" {
  description = "Bucket ID of s3 logging target."
  default     = null
  type        = map
}

variable "lambda_variables" {
  description = "Map of environment variables for lambda function."
  type        = map
  default     = null
}
