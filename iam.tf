resource "aws_iam_role" "speke_server_lambda_role" {
  count = var.mod_count

  name               = "${var.name}-LambdaSPEKERole"
  assume_role_policy = data.aws_iam_policy_document.speke_server_lambda_role_assume_policy.json
  tags               = local.tags
}

data "aws_iam_policy_document" "speke_server_lambda_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "speke_server_lambda_role_AWSLambdaBasicExecutionRole" {
  count = var.mod_count

  role       = join("", aws_iam_role.speke_server_lambda_role.*.name)
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_policy" "speke_server_key_bucket_policy" {
  count = var.mod_count

  name   = "${var.name}-SPEKEServerKeyBucketPolicy"
  policy = join("", data.aws_iam_policy_document.speke_server_key_bucket_policy.*.json)
}

data "aws_iam_policy_document" "speke_server_key_bucket_policy" {
  count = var.mod_count

  statement {
    actions = [
      "s3:PutObject",
      "s3:PutObjectAcl"
    ]
    resources = [
      "${join("", aws_s3_bucket.key_bucket.*.arn)}/*"
    ]
  }
}

resource "aws_iam_role_policy_attachment" "speke_server_lambda_role-speke_server_key_bucket_policy" {
  count = var.mod_count

  role       = join("", aws_iam_role.speke_server_lambda_role.*.name)
  policy_arn = join("", aws_iam_policy.speke_server_key_bucket_policy.*.arn)
}

resource "aws_iam_role" "media_package_invoke_speke_role" {
  count = var.mod_count

  name               = "${var.name}-MediaPackageInvokeSPEKERole"
  assume_role_policy = data.aws_iam_policy_document.media_package_invoke_speke_role_assume_policy.json
  tags               = local.tags
}

data "aws_iam_policy_document" "media_package_invoke_speke_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["mediapackage.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "media_package_invoke_speke_policy" {
  count = var.mod_count

  name   = "${var.name}-MediaPackageInvokeSPEKEPolicy"
  policy = join("", data.aws_iam_policy_document.media_package_invoke_speke_policy.*.json)
}

data "aws_iam_policy_document" "media_package_invoke_speke_policy" {
  count = var.mod_count

  statement {
    actions   = ["execute-api:Invoke"]
    resources = ["${join("", aws_api_gateway_deployment.speke_server_api_deployment.*.execution_arn)}/POST/copyProtection"]
  }
}

resource "aws_iam_role_policy_attachment" "media_package_invoke_speke_role-media_package_invoke_speke_policy" {
  count = var.mod_count

  role       = join("", aws_iam_role.media_package_invoke_speke_role.*.name)
  policy_arn = join("", aws_iam_policy.media_package_invoke_speke_policy.*.arn)
}

resource "aws_iam_role" "media_convert_invoke_speke_role" {
  count = var.mod_count

  name               = "${var.name}-MediaConvertInvokeSPEKERole"
  assume_role_policy = data.aws_iam_policy_document.media_convert_invoke_speke_role_assume_policy.json
  tags               = local.tags
}

data "aws_iam_policy_document" "media_convert_invoke_speke_role_assume_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "mediaconvert.amazonaws.com",
        "mediaconvert.us-east-1.amazonaws.com",
        "mediaconvert.ap-northeast-1.amazonaws.com",
        "mediaconvert.ap-southeast-1.amazonaws.com",
        "mediaconvert.ap-southeast-2.amazonaws.com",
        "mediaconvert.eu-central-1.amazonaws.com",
        "mediaconvert.eu-west-1.amazonaws.com",
        "mediaconvert.us-east-1.amazonaws.com",
        "mediaconvert.us-west-1.amazonaws.com",
        "mediaconvert.us-west-2.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_policy" "media_convert_invoke_speke_policy" {
  count = var.mod_count

  name   = "${var.name}-MediaConvertInvokeSPEKEPolicy"
  policy = join("", data.aws_iam_policy_document.media_convert_invoke_speke_policy.*.json)
}

data "aws_iam_policy_document" "media_convert_invoke_speke_policy" {
  count = var.mod_count

  statement {
    actions   = ["execute-api:Invoke"]
    resources = ["${join("", aws_api_gateway_deployment.speke_server_api_deployment.*.execution_arn)}/*/POST/copyProtection"]
  }
  statement {
    actions   = ["s3:*"]
    resources = ["*"]
  }
}

resource "aws_iam_role_policy_attachment" "media_convert_invoke_speke_role-media_convert_invoke_speke_policy" {
  count = var.mod_count

  role       = join("", aws_iam_role.media_convert_invoke_speke_role.*.name)
  policy_arn = join("", aws_iam_policy.media_convert_invoke_speke_policy.*.arn)
}

resource "aws_iam_role_policy_attachment" "speke_server_lambda_role-speke_secrets_manager_policy" {
  count = var.mod_count

  role       = join("", aws_iam_role.speke_server_lambda_role.*.name)
  policy_arn = join("", aws_iam_policy.speke_secrets_manager_policy.*.arn)
}

resource "aws_iam_policy" "speke_secrets_manager_policy" {
  count = var.mod_count

  name   = "${var.name}-SPEKESecretsManagerPolicy"
  policy = join("", data.aws_iam_policy_document.speke_secrets_manager_policy.*.json)
}

data "aws_iam_policy_document" "speke_secrets_manager_policy" {
  count = var.mod_count

  statement {
    actions   = ["secretsmanager:GetSecretValue"]
    resources = ["arn:aws:secretsmanager:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:secret:speke/*"]
  }

  statement {
    actions = [
      "secretsmanager:GetRandomPassword",
      "secretsmanager:CreateSecret"
    ]
    resources = ["*"]
  }
}
