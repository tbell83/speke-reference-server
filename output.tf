output "key_bucket" {
  value       = aws_s3_bucket.key_bucket
  description = "Bucket name for the SPEKE key cache"
}

output "media_package_speke_role" {
  value       = aws_iam_role.media_package_invoke_speke_role
  description = "Media package SPEKE role resource."
}

output "media_convert_speke_role" {
  value       = aws_iam_role.media_convert_invoke_speke_role
  description = "Media Convert SPEKE role resource."
}

output "lambda_speke_role" {
  value       = aws_iam_role.speke_server_lambda_role
  description = "Lambda SPEKE role resource."
}

output "speke_server_url" {
  value       = local.speke_server_url
  description = "URL for the SPEKE server that is called by MediaPackage."
}
