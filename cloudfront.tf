resource "aws_cloudfront_distribution" "key_bucket_cloudfront_distribution" {
  count = var.mod_count

  comment     = "SPEKE CDN for ${join("", aws_s3_bucket.key_bucket.*.id)}"
  enabled     = true
  price_class = "PriceClass_All"

  default_cache_behavior {
    target_origin_id       = "S3_${join("", aws_s3_bucket.key_bucket.*.id)}"
    viewer_protocol_policy = "https-only"
    min_ttl                = 0
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  origin {
    domain_name = join("", aws_s3_bucket.key_bucket.*.bucket_domain_name)
    origin_id   = "S3_${join("", aws_s3_bucket.key_bucket.*.id)}"

    s3_origin_config {
      origin_access_identity = join("", aws_cloudfront_origin_access_identity.key_bucket_origin_access_identity.*.cloudfront_access_identity_path)
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
      locations        = []
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

resource "aws_cloudfront_origin_access_identity" "key_bucket_origin_access_identity" {
  count = var.mod_count

  comment = "SPEKE Origin Access Identity for ${join("", aws_s3_bucket.key_bucket.*.id)}"
}
