resource "aws_s3_bucket" "key_bucket" {
  count = var.mod_count

  bucket = lower("${var.name}-speke")

  lifecycle_rule {
    id      = "Expire old keys"
    enabled = true
    expiration {
      days = var.key_retention_days
    }
  }

  dynamic "logging" {
    for_each = var.s3_logging != null ? [var.s3_logging] : []
    content {
      target_bucket = lookup(logging.value, "target_bucket", null)
      target_prefix = lookup(logging.value, "target_prefix", null)
    }
  }

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    max_age_seconds = 3000
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = local.tags
}

resource "aws_s3_bucket_policy" "key_bucket_policy" {
  count = var.mod_count

  bucket = join("", aws_s3_bucket.key_bucket.*.id)
  policy = join("", data.aws_iam_policy_document.key_bucket_policy.*.json)
}

data "aws_iam_policy_document" "key_bucket_policy" {
  count = var.mod_count

  statement {
    actions   = ["s3:GetObject"]
    resources = ["${join("", aws_s3_bucket.key_bucket.*.arn)}/*"]
    principals {
      type        = "AWS"
      identifiers = [join("", aws_cloudfront_origin_access_identity.key_bucket_origin_access_identity.*.iam_arn)]
    }
  }

  statement {
    sid    = "ForceSSLOnlyAccess"
    effect = "Deny"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions   = ["s3:*"]
    resources = ["${join("", aws_s3_bucket.key_bucket.*.arn)}/*"]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"
      values   = ["false"]
    }
  }
}
