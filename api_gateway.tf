resource "aws_api_gateway_rest_api" "speke_server_api_gateway" {
  count = var.mod_count

  name   = "${var.name}-SPEKEReferenceAPI"
  policy = join("", data.aws_iam_policy_document.speke_gateway_resource_policy.*.json)

  depends_on = ["aws_iam_role.media_convert_invoke_speke_role", "aws_iam_role.media_package_invoke_speke_role"]
}

resource "aws_api_gateway_deployment" "speke_server_api_deployment" {
  count = var.mod_count

  description = "Default stage deployment for SPEKE Server API"
  rest_api_id = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  stage_name  = var.stage_name

  depends_on = ["aws_api_gateway_integration.integration"]
}

resource "aws_api_gateway_integration" "integration" {
  count = var.mod_count

  content_handling        = "CONVERT_TO_TEXT"
  rest_api_id             = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id             = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method             = join("", aws_api_gateway_method.gateway_method.*.http_method)
  integration_http_method = "POST"
  passthrough_behavior    = "WHEN_NO_MATCH"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/${join("", aws_lambda_function.speke_server_lambda.*.arn)}/invocations"
}

resource "aws_api_gateway_resource" "copy_protection" {
  count = var.mod_count

  rest_api_id = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  parent_id   = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.root_resource_id)
  path_part   = "copyProtection"
}

resource "aws_api_gateway_method" "gateway_method" {
  count = var.mod_count

  rest_api_id   = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id   = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "resp_200" {
  count = var.mod_count

  rest_api_id = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method = join("", aws_api_gateway_method.gateway_method.*.http_method)
  status_code = "200"
  response_parameters = {
    "method.response.header.Speke-User-Agent" = false
  }
}

resource "aws_api_gateway_integration_response" "resp_200" {
  count = var.mod_count

  rest_api_id = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method = join("", aws_api_gateway_method.gateway_method.*.http_method)
  status_code = join("", aws_api_gateway_method_response.resp_200.*.status_code)
  response_parameters = {
    "method.response.header.Speke-User-Agent" = "integration.response.header.Speke-User-Agent"
  }

  depends_on = ["aws_api_gateway_integration.integration"]
}

resource "aws_api_gateway_method_response" "resp_500" {
  count = var.mod_count

  rest_api_id = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method = join("", aws_api_gateway_method.gateway_method.*.http_method)
  status_code = "500"
}

resource "aws_api_gateway_integration_response" "resp_500" {
  count = var.mod_count

  rest_api_id       = join("", aws_api_gateway_rest_api.speke_server_api_gateway.*.id)
  resource_id       = join("", aws_api_gateway_resource.copy_protection.*.id)
  http_method       = join("", aws_api_gateway_method.gateway_method.*.http_method)
  status_code       = join("", aws_api_gateway_method_response.resp_500.*.status_code)
  selection_pattern = "Error.*"

  depends_on = ["aws_api_gateway_integration.integration"]
}

data "aws_iam_policy_document" "speke_gateway_resource_policy" {
  count = var.mod_count

  statement {
    actions = ["execute-api:Invoke"]
    principals {
      type = "AWS"
      identifiers = concat(
        [
          join("", aws_iam_role.media_convert_invoke_speke_role.*.arn),
          join("", aws_iam_role.media_package_invoke_speke_role.*.arn),
        ],
        var.invoke_gateway_arns
      )
    }

    resources = [
      "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*/${var.stage_name}/POST/copyProtection"
    ]
  }
}
